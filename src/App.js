import logo from "./logo.svg";
import "./App.css";
import Ex_Layout from "./BaiTapLayoutComponent/Ex_Layout";

function App() {
  return (
    <div className="App">
      <Ex_Layout />
    </div>
  );
}

export default App;
