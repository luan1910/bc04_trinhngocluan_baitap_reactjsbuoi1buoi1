import React, { Component } from "react";
import BannerComponent from "./BannerComponent";

import FooterComponent from "./FooterComponent";
import HeaderComponent from "./HeaderComponent";
import ItemComponent from "./ItemComponent";

export default class Ex_Layout extends Component {
  render() {
    return (
      <div>
        <HeaderComponent />
        <BannerComponent />
        <ItemComponent />
        <FooterComponent />
      </div>
    );
  }
}
